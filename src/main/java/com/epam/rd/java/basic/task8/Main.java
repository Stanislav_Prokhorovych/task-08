package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.entity.Flowers;
import com.epam.rd.java.basic.task8.validation.Validation;


public class Main {

	public static void main(String[] args) throws Exception {
		if (args.length == 0) {
			return;
		}

		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);

		String xsdFileName = null;
		if (args.length > 1) {
			xsdFileName = args[1];
			System.out.println("XSD ==> " + xsdFileName);
		}
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////

		// get container
		DOMController domController = new DOMController(xmlFileName);
		// PLACE YOUR CODE HERE
		Flowers flowersDOM = domController.parseFromXML();

		// sort (case 1)
		// PLACE YOUR CODE HERE
		flowersDOM.sortByFlowerName();
		// save
		String outputXmlFile = "output.dom.xml";
		// PLACE YOUR CODE HERE
		domController.parseToXML(flowersDOM, outputXmlFile);
		boolean isValidDOM = Validation.validateXML(outputXmlFile, xsdFileName);
		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////

		// get
		SAXController saxController = new SAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		Flowers flowersSAX = saxController.parseFromXML();

		// sort  (case 2)
		// PLACE YOUR CODE HERE
		flowersSAX.sortByFlowerOrigin();
		// save
		outputXmlFile = "output.sax.xml";
		// PLACE YOUR CODE HERE
		saxController.parseToXML(flowersSAX, outputXmlFile);
		boolean isValidSAX = Validation.validateXML(outputXmlFile, xsdFileName);
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////

		// get
		STAXController staxController = new STAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		Flowers flowersSTAX = staxController.parseFromXML();

		// sort  (case 3)
		// PLACE YOUR CODE HERE
		flowersSTAX.sortByAveLenFlower();
		// save
		outputXmlFile = "output.stax.xml";
		// PLACE YOUR CODE HERE
		staxController.parseToXML(flowersSTAX, outputXmlFile);
		boolean isValidStAX = Validation.validateXML(outputXmlFile, xsdFileName);
	}
}
