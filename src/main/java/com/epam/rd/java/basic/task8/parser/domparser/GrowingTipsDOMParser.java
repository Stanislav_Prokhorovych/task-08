package com.epam.rd.java.basic.task8.parser.domparser;

import com.epam.rd.java.basic.task8.entity.GrowingTips;
import com.epam.rd.java.basic.task8.entity.Temperature;
import com.epam.rd.java.basic.task8.entity.Watering;
import com.epam.rd.java.basic.task8.constants.GrowingTipsFields;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class GrowingTipsDOMParser {

    public static GrowingTips parseFromXML(Element flowerElement) {
        Node node = flowerElement.getElementsByTagName(GrowingTipsFields.NODE_NAME).item(0);
        Element element = (Element) node;

        Temperature temperature = TemperatureDOMParser.parseFromXML(element);

        Node nodeLighting = element.getElementsByTagName(GrowingTipsFields.ELEMENT_LIGHTING).item(0);
        String lightRequiring = nodeLighting.getAttributes().getNamedItem(GrowingTipsFields.ATTR_LIGHT_REQUIRING).getTextContent();

        Watering watering = WateringDOMParser.parseFromXML(element);

        return new GrowingTips(temperature, lightRequiring, watering);
    }

    public static Element parseToXML(Document document, GrowingTips growingTips) {
        Element growingTipsElement = document.createElement(GrowingTipsFields.NODE_NAME);

        Element temperature = TemperatureDOMParser.parseToXML(document, growingTips.getTemperature());
        Element lighting = document.createElement(GrowingTipsFields.ELEMENT_LIGHTING);
        Element watering = WateringDOMParser.parseToXML(document, growingTips.getWatering());

        lighting.setAttribute(GrowingTipsFields.ATTR_LIGHT_REQUIRING, growingTips.getLighting());

        growingTipsElement.appendChild(temperature);
        growingTipsElement.appendChild(lighting);
        growingTipsElement.appendChild(watering);

        return growingTipsElement;
    }
}
