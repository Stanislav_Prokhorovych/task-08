package com.epam.rd.java.basic.task8.parser.staxparser;

import com.epam.rd.java.basic.task8.entity.GrowingTips;
import com.epam.rd.java.basic.task8.entity.Temperature;
import com.epam.rd.java.basic.task8.entity.Watering;
import com.epam.rd.java.basic.task8.constants.GrowingTipsFields;
import com.epam.rd.java.basic.task8.constants.TemperatureFields;
import com.epam.rd.java.basic.task8.constants.WateringFields;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLStreamException;

public class GrowingTipsStAXParser extends AbstractStAXParser implements StAXParser<GrowingTips> {

    public GrowingTipsStAXParser(XMLEventWriter writer, XMLEventFactory eventFactory) {
        super(writer, eventFactory);
    }

    @Override
    public void parseToXML(GrowingTips growingTips) throws XMLStreamException {
        createStartElement(GrowingTipsFields.NODE_NAME);
        createStartElement(TemperatureFields.NODE_NAME);

        Temperature temperature = growingTips.getTemperature();
        createAttribute(TemperatureFields.ATTR_MEASURE, temperature.getMeasure());
        createCharacters(temperature.getValue());
        createEndElement(TemperatureFields.NODE_NAME);
        createStartElement(GrowingTipsFields.ELEMENT_LIGHTING);
        createAttribute(GrowingTipsFields.ATTR_LIGHT_REQUIRING, growingTips.getLighting());
        createEndElement(GrowingTipsFields.ELEMENT_LIGHTING);
        createStartElement(WateringFields.NODE_NAME);

        Watering watering = growingTips.getWatering();
        createAttribute(WateringFields.ATTR_MEASURE, watering.getMeasure());
        createCharacters(watering.getValue());
        createEndElement(WateringFields.NODE_NAME);
        createEndElement(GrowingTipsFields.NODE_NAME);
    }
}
