package com.epam.rd.java.basic.task8.parser.domparser;

import com.epam.rd.java.basic.task8.entity.AveLenFlower;
import com.epam.rd.java.basic.task8.entity.VisualParameters;
import com.epam.rd.java.basic.task8.constants.VisualParametersFields;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class VisualParametersDOMParser {

    public static VisualParameters parseFromXML(Element flowerElement) {
        Node node = flowerElement.getElementsByTagName(VisualParametersFields.NODE_NAME).item(0);
        Element element = (Element) node;

        String stemColour = element.getElementsByTagName(VisualParametersFields.ELEMENT_STEM_COLOUR).item(0).getTextContent();
        String leafColour = element.getElementsByTagName(VisualParametersFields.ELEMENT_LEAF_COLOUR).item(0).getTextContent();

        AveLenFlower aveLenFlower = AveLenFlowerDOMParser.parseFromXML(element);

        return new VisualParameters(stemColour, leafColour, aveLenFlower);
    }

    public static Element parseToXML(Document document, VisualParameters visualParameters) {
        Element visualParametersElement = document.createElement(VisualParametersFields.NODE_NAME);

        Element stemColour = document.createElement(VisualParametersFields.ELEMENT_STEM_COLOUR);
        Element leafColour = document.createElement(VisualParametersFields.ELEMENT_LEAF_COLOUR);

        stemColour.setTextContent(visualParameters.getStemColour());
        leafColour.setTextContent(visualParameters.getLeafColour());

        visualParametersElement.appendChild(stemColour);
        visualParametersElement.appendChild(leafColour);
        visualParametersElement.appendChild(
                AveLenFlowerDOMParser.parseToXML(document, visualParameters.getAveLenFlower()));

        return visualParametersElement;
    }
}
