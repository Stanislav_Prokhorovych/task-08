package com.epam.rd.java.basic.task8.entity;

public class VisualParameters {

    private String StemColour;
    private String LeafColour;
    private AveLenFlower aveLenFlower;

    public VisualParameters() {
    }

    public VisualParameters(String stemColour, String leafColour, AveLenFlower aveLenFlower) {
        StemColour = stemColour;
        LeafColour = leafColour;
        this.aveLenFlower = aveLenFlower;
    }

    public String getStemColour() {
        return StemColour;
    }

    public void setStemColour(String stemColour) {
        StemColour = stemColour;
    }

    public String getLeafColour() {
        return LeafColour;
    }

    public void setLeafColour(String leafColour) {
        LeafColour = leafColour;
    }

    public AveLenFlower getAveLenFlower() {
        return aveLenFlower;
    }

    public void setAveLenFlower(AveLenFlower aveLenFlower) {
        this.aveLenFlower = aveLenFlower;
    }

    @Override
    public String toString() {
        return "VisualParameters{" +
                "StemColour='" + StemColour + "'" +
                ",\n LeafColour='" + LeafColour + "'" +
                ",\n aveLenFlower=" + aveLenFlower +
                '}';
    }
}
