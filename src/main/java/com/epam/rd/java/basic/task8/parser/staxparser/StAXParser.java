package com.epam.rd.java.basic.task8.parser.staxparser;

import javax.xml.stream.XMLStreamException;

public interface StAXParser<E> {
    void parseToXML(E e) throws XMLStreamException;
}

