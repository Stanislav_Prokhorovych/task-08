package com.epam.rd.java.basic.task8.constants;

public interface AveLenFlowerFields {
    String NODE_NAME = "aveLenFlower";
    String ATTR_MEASURE = "measure";
}
