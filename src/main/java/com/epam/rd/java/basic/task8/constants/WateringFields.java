package com.epam.rd.java.basic.task8.constants;

public interface WateringFields {
    String NODE_NAME = "watering";
    String ATTR_MEASURE = "measure";
}
