package com.epam.rd.java.basic.task8.parser.saxparser;

import javax.xml.stream.XMLStreamException;

public interface CustomSAXParser<E> {
    void toXML(E e) throws XMLStreamException;
}
