package com.epam.rd.java.basic.task8.constants;

public interface VisualParametersFields {
    String NODE_NAME = "visualParameters";
    String ELEMENT_STEM_COLOUR = "stemColour";
    String ELEMENT_LEAF_COLOUR = "leafColour";
}
