package com.epam.rd.java.basic.task8.entity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Flowers {

    private final Map<String, String> attributes;
    private final List<Flower> flowers;

    public Flowers() {
        this.attributes = new LinkedHashMap<>();
        this.flowers = new ArrayList<>();
    }

    public void addFlower(Flower flower) {
        flowers.add(flower);
    }

    public List<Flower> getFlowers() {
        return flowers;
    }

    public void addAttribute(String name, String value) {
        this.attributes.put(name, value);
    }

    public Map<String, String> getAllAttributes() {
        return this.attributes;
    }

    public void sortByFlowerName() {
        flowers.sort(Comparator.comparing(Flower::getName));
    }

    public void sortByFlowerOrigin() {
        flowers.sort(Comparator.comparing(Flower::getOrigin));
    }

    public void sortByAveLenFlower() {
        flowers.sort(Comparator.comparingInt(o -> o.getVisualParameters().getAveLenFlower().getValue()));
    }
}
