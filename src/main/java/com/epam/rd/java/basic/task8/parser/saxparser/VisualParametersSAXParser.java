package com.epam.rd.java.basic.task8.parser.saxparser;

import com.epam.rd.java.basic.task8.entity.AveLenFlower;
import com.epam.rd.java.basic.task8.entity.VisualParameters;
import com.epam.rd.java.basic.task8.constants.AveLenFlowerFields;
import com.epam.rd.java.basic.task8.constants.VisualParametersFields;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

public class VisualParametersSAXParser extends AbstractSAXParser implements CustomSAXParser<VisualParameters> {

    public VisualParametersSAXParser(XMLStreamWriter writer) {
        super(writer);
    }

    @Override
    public void toXML(VisualParameters visualParameters) throws XMLStreamException {
        writeStartElement(VisualParametersFields.NODE_NAME);
        writeStartElement(VisualParametersFields.ELEMENT_STEM_COLOUR);
        writeCharacters(visualParameters.getStemColour());
        writeEndElement();
        writeStartElement(VisualParametersFields.ELEMENT_LEAF_COLOUR);
        writeCharacters(visualParameters.getLeafColour());
        writeEndElement();
        writeStartElement(AveLenFlowerFields.NODE_NAME);
        AveLenFlower aveLenFlower = visualParameters.getAveLenFlower();
        writeAttribute(AveLenFlowerFields.ATTR_MEASURE, aveLenFlower.getMeasure());
        writeCharacters(String.valueOf(aveLenFlower.getValue()));
        writeEndElement();
        writeEndElement();
    }
}
