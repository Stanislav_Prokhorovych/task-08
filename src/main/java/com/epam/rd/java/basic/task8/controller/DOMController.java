package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entity.Flowers;
import com.epam.rd.java.basic.task8.parser.domparser.FlowersDOMParser;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Controller for DOM parser.
 */
public class DOMController  {

    private final String xmlFileName;

    public DOMController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    public Flowers parseFromXML() {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        Flowers flowers = null;

        try {
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document document = db.parse(new File(xmlFileName));

            flowers = FlowersDOMParser.parseFromXML(document.getDocumentElement());
        } catch (ParserConfigurationException | IOException | SAXException e) {
            e.printStackTrace();
        }
        return flowers;
    }

    public void parseToXML(Flowers flowers, String outputXmlFile)  {
        try (FileOutputStream outputStream = new FileOutputStream(outputXmlFile)) {
            Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();

            Element rootElement = FlowersDOMParser.parseToXML(document, flowers);
            document.appendChild(rootElement);

            writeXML(document, outputStream);

        } catch (IOException | ParserConfigurationException e) {
            e.printStackTrace();
        }
    }

    private void writeXML(Document document, FileOutputStream outputStream) {
        try {
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");

            DOMSource source = new DOMSource(document);
            StreamResult result = new StreamResult(outputStream);

            transformer.transform(source, result);
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }
}
