package com.epam.rd.java.basic.task8.constants;

public interface FlowerFields {
    String NODE_NAME = "flower";
    String ELEMENT_NAME = "name";
    String ELEMENT_SOIL = "soil";
    String ELEMENT_ORIGIN = "origin";
    String ELEMENT_MULTIPLYING = "multiplying";
}
