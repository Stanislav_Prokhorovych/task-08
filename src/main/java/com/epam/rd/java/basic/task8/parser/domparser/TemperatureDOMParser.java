package com.epam.rd.java.basic.task8.parser.domparser;

import com.epam.rd.java.basic.task8.entity.Temperature;
import com.epam.rd.java.basic.task8.constants.TemperatureFields;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class TemperatureDOMParser {

    public static Temperature parseFromXML(Element growingTips) {
        Node node = growingTips.getElementsByTagName(TemperatureFields.NODE_NAME).item(0);

        String measure = node.getAttributes().getNamedItem(TemperatureFields.ATTR_MEASURE).getTextContent();
        int value = Integer.parseInt(node.getTextContent());

        return new Temperature(measure, value);
    }

    public static Element parseToXML(Document document, Temperature temperature) {
        Element visualParametersElement = document.createElement(TemperatureFields.NODE_NAME);
        visualParametersElement.setAttribute(TemperatureFields.ATTR_MEASURE, temperature.getMeasure());
        visualParametersElement.setTextContent(String.valueOf(temperature.getValue()));

        return visualParametersElement;
    }
}
