package com.epam.rd.java.basic.task8.parser.staxparser;

import com.epam.rd.java.basic.task8.entity.AveLenFlower;
import com.epam.rd.java.basic.task8.entity.VisualParameters;
import com.epam.rd.java.basic.task8.constants.AveLenFlowerFields;
import com.epam.rd.java.basic.task8.constants.VisualParametersFields;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLStreamException;

public class VisualParametersStAXParser extends AbstractStAXParser implements StAXParser<VisualParameters> {

    public VisualParametersStAXParser(XMLEventWriter writer, XMLEventFactory eventFactory) {
        super(writer, eventFactory);
    }

    @Override
    public void parseToXML(VisualParameters visualParameters) throws XMLStreamException {
        createStartElement(VisualParametersFields.NODE_NAME);
        createStartElement(VisualParametersFields.ELEMENT_STEM_COLOUR);
        createCharacters(visualParameters.getStemColour());
        createEndElement(VisualParametersFields.ELEMENT_STEM_COLOUR);
        createStartElement(VisualParametersFields.ELEMENT_LEAF_COLOUR);
        createCharacters(visualParameters.getLeafColour());
        createEndElement(VisualParametersFields.ELEMENT_LEAF_COLOUR);
        createStartElement(AveLenFlowerFields.NODE_NAME);
        AveLenFlower aveLenFlower = visualParameters.getAveLenFlower();
        createAttribute(AveLenFlowerFields.ATTR_MEASURE, aveLenFlower.getMeasure());
        createCharacters(String.valueOf(aveLenFlower.getValue()));
        createEndElement(AveLenFlowerFields.NODE_NAME);
        createEndElement(VisualParametersFields.NODE_NAME);
    }
}
