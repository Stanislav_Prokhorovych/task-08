package com.epam.rd.java.basic.task8.parser.domparser;

import com.epam.rd.java.basic.task8.entity.AveLenFlower;
import com.epam.rd.java.basic.task8.constants.AveLenFlowerFields;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class AveLenFlowerDOMParser {

    public static AveLenFlower parseFromXML(Element visualElement) {
        Node node = visualElement.getElementsByTagName(AveLenFlowerFields.NODE_NAME).item(0);

        String aveLenFlowerMeasure = node.getAttributes().getNamedItem(AveLenFlowerFields.ATTR_MEASURE).getTextContent();
        int aveLenFlowerValue = Integer.parseInt(node.getTextContent());

        return new AveLenFlower(aveLenFlowerMeasure, aveLenFlowerValue);
    }

    public static Element parseToXML(Document document, AveLenFlower aveLenFlower) {
        Element element = document.createElement(AveLenFlowerFields.NODE_NAME);
        element.setAttribute(AveLenFlowerFields.ATTR_MEASURE, aveLenFlower.getMeasure());
        element.setTextContent(String.valueOf(aveLenFlower.getValue()));

        return element;
    }
}
