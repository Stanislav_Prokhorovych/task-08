package com.epam.rd.java.basic.task8.parser.saxparser;

import com.epam.rd.java.basic.task8.entity.GrowingTips;
import com.epam.rd.java.basic.task8.entity.Temperature;
import com.epam.rd.java.basic.task8.entity.Watering;
import com.epam.rd.java.basic.task8.constants.GrowingTipsFields;
import com.epam.rd.java.basic.task8.constants.TemperatureFields;
import com.epam.rd.java.basic.task8.constants.WateringFields;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

public class GrowingTipsSAXParser extends AbstractSAXParser implements CustomSAXParser<GrowingTips> {

    public GrowingTipsSAXParser(XMLStreamWriter writer) {
        super(writer);
    }

    @Override
    public void toXML(GrowingTips growingTips) throws XMLStreamException {
        writeStartElement(GrowingTipsFields.NODE_NAME);
        writeStartElement(TemperatureFields.NODE_NAME);

        Temperature temperature = growingTips.getTemperature();
        writeAttribute(TemperatureFields.ATTR_MEASURE, temperature.getMeasure());
        writeCharacters(temperature.getValue());
        writeEndElement();
        writeStartElement(GrowingTipsFields.ELEMENT_LIGHTING);
        writeAttribute(GrowingTipsFields.ATTR_LIGHT_REQUIRING, growingTips.getLighting());
        writeEndElement();
        writeStartElement(WateringFields.NODE_NAME);

        Watering watering = growingTips.getWatering();
        writeAttribute(WateringFields.ATTR_MEASURE, watering.getMeasure());
        writeCharacters(watering.getValue());
        writeEndElement();
        writeEndElement();
    }
}
