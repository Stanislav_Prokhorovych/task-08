package com.epam.rd.java.basic.task8.parser.staxparser;

import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.entity.Flowers;
import com.epam.rd.java.basic.task8.constants.FlowersFields;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLStreamException;
import java.util.Map;

public class FlowersStAXParser extends AbstractStAXParser implements StAXParser<Flowers> {

    public FlowersStAXParser(XMLEventWriter writer, XMLEventFactory eventFactory) {
        super(writer, eventFactory);
    }

    @Override
    public void parseToXML(Flowers flowers) throws XMLStreamException {
        createStartElement(FlowersFields.NODE_NAME);

        for(Map.Entry<String, String> entry: flowers.getAllAttributes().entrySet()) {
            createAttribute(entry.getKey(), entry.getValue());
        }

        StAXParser<Flower> flowerSAXParser = new FlowerStAXParser(writer, eventFactory);
        for (Flower flower: flowers.getFlowers()) {
            flowerSAXParser.parseToXML(flower);
        }

        createEndElement(FlowersFields.NODE_NAME);
    }
}
