package com.epam.rd.java.basic.task8.entity;

public class AveLenFlower {

    private String measure;
    private int value;

    public AveLenFlower() {
    }

    public AveLenFlower(String measure, int value) {
        this.measure = measure;
        this.value = value;
    }

    public String getMeasure() {
        return measure;
    }

    public void setMeasure(String measure) {
        this.measure = measure;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "AveLenFlower{" +
                "measure='" + measure + "'" +
                ",\n value='" + value + "'" +
                '}';
    }
}
